#include <QCoreApplication>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

#include <QDebug>
#include <QThread>
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QSerialPort serial    ;
    QByteArray binByteArray;
    int        dataLen=0;
    //QByteArray databuf;
    //QByteArray qba = QString::fromStdString("STX12\r\n").toAscii();
    QString portName;

    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        qDebug() << "Name        : " << info.portName();
        qDebug() << "Description : " << info.description();
        qDebug() << "Manufacturer: " << info.manufacturer();
        portName=info.portName();
        // Example use QSerialPort
        //QSerialPort serial;
        serial.setPort(info);
        if (serial.open(QIODevice::ReadWrite))
            serial.close();
    }


    serial.setPortName(portName);
    qDebug() << "Set Port "<<portName;

    //serial.setPortName("ttyUSB1");

    if(!serial.setBaudRate((QSerialPort::Baud115200),QSerialPort::AllDirections))

        //if(!serial.setBaudRate(QSerialPort::Baud115200 , QSerialPort::Input))
        qDebug() << serial.errorString();
    if(!serial.setDataBits(QSerialPort::Data8))
        qDebug() << serial.errorString();
    if(!serial.setParity(QSerialPort::NoParity))
        qDebug() << serial.errorString();
    if(!serial.setFlowControl(QSerialPort::NoFlowControl))
        qDebug() << serial.errorString();
    if(!serial.setStopBits(QSerialPort::OneStop))
        qDebug() << serial.errorString();
    if(!serial.open(QIODevice::ReadWrite))
        qDebug() << serial.errorString();




    //qDebug() << serial.bytesAvailable();
    char databuf[32];
    while(true)
    {
        QThread::usleep(3000000);

        // sleep(4000);
        if (serial.isOpen()) {
            sprintf(databuf,"SRX+2\r\n");
            binByteArray.clear();
            binByteArray.append(databuf);

            //  serial.write(binByteArray);
            //            qDebug() << databuf;
            QThread::usleep(300000);

            sprintf(databuf,"STX2\r\n");
            binByteArray.clear();
            binByteArray.append(databuf);


            //serial.write(binByteArray);




            serial.write( QString("SRX+1\r\n").toLocal8Bit() );    //写入随机值
            if(serial.waitForBytesWritten(1000))  //100ms 等待写入成功
            {
                if(serial.waitForReadyRead(1000))  //等待数据返回
                {
                    QString str((serial.readAll()));
                    QStringList  list = str.split(QRegExp("[\r\n]"),QString::SkipEmptyParts);      //去掉\r\n

                    foreach (QString line, list) {

                        qDebug()<<line;

                        //解析line,并向界面发送信号 ... ...
                    }
                }
                else
                    qDebug()<<"read err";
            }
            else
                qDebug()<<"write err";




            //qDebug() << binByteArray;
            QByteArray datas = serial.readAll();
            //  QByteArray datas=(serial.readAll()).toHex();

            if (datas.size() != 0) {
                //    qDebug() << "Arrived data: 0";
                //} else {
                for (int i = 0; i < datas.size(); i++){
                    if (datas.at(i)) {
                        qDebug()  << datas[i];
                    }
                }
            }

        } else {
            qDebug() << "OPEN ERROR: " << serial.errorString();
        }
    }
    //return 0;
    return a.exec();
}
